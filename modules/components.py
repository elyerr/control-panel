# -*- coding: utf-8 -*-
"""
Created on Sun May  8 23:21:31 2022

@author: Elyerr
"""

import tkinter as tk
from tkinter import ttk
from modules.core import System
from modules.color import Color

class Components():

    @staticmethod 
    def components(**kwargs):
        """Plantilla de componentes
        
        Args:
            master (Frame): Frame padre.
            name (str): Nombre del Programa.
            deamon (str): Demonio del programa.
            output (Tk.text): Caja de texto para la salida de informacion.
        """
        ############# Style Contenedores ################
        Color.set_group_color(name="TFrame", bg=Color.dark, fg=Color.white, relief="flat")
        Color.set_group_color(name="TLabel", bg=Color.dark, fg=Color.white, relief="flat")
         
        #############Contenedor principal#########
        container = ttk.Frame(kwargs['master'])
        container.pack(expand=True, fill="x")

        ##############Contenedor del nombre del programa ###################
        #self.configure(background="") 
        panel_label = ttk.Frame(container)
        panel_label.grid(row=0, column=0)
        
        #############Contenedor para almacenar los botones###################
        panel_button = ttk.Frame(container)
        panel_button.grid(row=0, column=1)

        ##############Nombre del programa##############################
        label = ttk.Label(panel_label, text=kwargs['name'], width=18)
        label.pack()
        label.configure(justify="right", anchor="center")
                
        ################### configuracion boton status###########################
        button_status = ttk.Button(panel_button, text="Status")
        button_status['command'] = lambda: System.run_service(
            deamon=kwargs['deamon'], action="status", button=label_status, output=kwargs['output'])

        button_status.pack(side="left", padx=5)

        ################### configuracion boton start###########################
        button_start = ttk.Button(panel_button, text="Start")
        button_start['command'] = lambda: System.run_service(
            deamon=kwargs['deamon'], action="start", button=label_status, output=kwargs['output'])

        button_start.pack(side="left", padx=5)

        ################### configuracion boton Restart###########################
        button_restart = ttk.Button(panel_button, text="Restart")
        button_restart['command'] = lambda: System.run_service(
            deamon=kwargs['deamon'], action="restart", button=label_status, output=kwargs['output'])

        button_restart.pack(side="left", padx=5)

        ################### configuracion boton Stop###########################
        button_stop = ttk.Button(panel_button, text="Stop")
        button_stop['command'] = lambda: System.run_service(
            deamon=kwargs['deamon'], action="stop", button=label_status, output=kwargs['output'])

        button_stop.pack(side="left", padx=5)

        ################### configuracion boton Enable###########################
        button_enable = ttk.Button(panel_button, text="Enable")
        button_enable['command'] = lambda: System.run_service(
            deamon=kwargs['deamon'], action="enable", button=label_status, output=kwargs['output'])
        
        button_enable.pack(side="left", padx=5)

        ################### configuracion boton Disable###########################

        button_disable = ttk.Button(panel_button, text="Disable")
        button_disable['command'] = lambda: System.run_service(
            deamon=kwargs['deamon'], action="disable", button=label_status, output=kwargs['output'])

        button_disable.pack(side="left", padx=5)

        ################### configuracion boton Stado###########################

        label_status = ttk.Label(panel_button)
        label_status.pack(side="right", ipadx=10)

        #botones para habilitar o deshabilitar
        buttons = [button_start, button_disable, button_status,
                   button_enable, button_restart, button_stop]
        #verifica que el programa exista
        System.verifyProgram(deamon=kwargs["deamon"], buttons=buttons)
        #informacion de estado del programa
        System.status(deamon=kwargs['deamon'], button=label_status)
        
        
    

    @staticmethod
    def output(master):
        """Funcion para la creacion del componente que muestra la informacion

        Args:
            master (Frame): Frame padre

        Returns:
            tk.Text: Retorna el componente hijo
        """
        ############Contenero para los componentes############
        panel = tk.Frame(master, borderwidth=1, relief="solid")
        panel.pack(expand=True, fill="x", side="bottom")

        ###############Componente para salida de informacion############
        output = tk.Text(panel)
        
        ############# Scrollbar y ################################
        scrolly = ttk.Scrollbar(panel, orient="vertical", command=output.yview)
        scrolly.pack(side="right", fill="y")
        
        ################Scrollbar x ###############################
        scrollx = ttk.Scrollbar(
            panel, orient="horizontal", command=output.xview)
        scrollx.pack(side="bottom", expand=True, fill="x")
        
        ##########Configuracion de componente de salidad de informacion #############
        output.pack(expand=True, fill="x")
        output.config(height=12, wrap='none',
                      xscrollcommand=scrollx.set, yscrollcommand=scrolly.set)

        return output
