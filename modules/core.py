# -*- coding: utf-8 -*-
"""
Created on Tue Feb 23 23:57:00 2021

@author: Elyerr
"""

import subprocess as sp 
import shutil
import tkinter as tk 

class System():
    # service_name, service_action, status, output=None
    @staticmethod
    def run_service(**kwargs):
        command = ["systemctl", kwargs['action'], kwargs['deamon']]        
        
        out = sp.run(command, capture_output=True, text=True)             
        kwargs['output'].delete(0.1, tk.END)
        if out.stderr:
            kwargs['output'].insert(tk.INSERT, f'{out.stderr}')
        else:
            kwargs['output'].insert(tk.INSERT, f'{out.stdout}')
        
        System.status(deamon = kwargs['deamon'], button = kwargs['button']) 

   

    @staticmethod
    def is_installed(package_name):
        """Comprueba si un programa esta instalado

        Args:
            package_name (_String_): Demonio del programa

        Returns:
            _Boolean_: Retorna un valor buoleano
        """
        return shutil.which(package_name) is not None
     
 
    @staticmethod
    def close(event, parent):
        """Escucha un evento 

        Args:
            event (_type_): Evento
            parent (_type_): Frame principal
        """
        if event.keysym == "Escape":
            parent.destroy()

    
    @staticmethod
    def verifyProgram(**kwargs):
        """Verifica 
        """
        try:
            result = sp.run(
                ["systemctl", "is-active", kwargs['deamon']], capture_output=True, text=True)
            if result.returncode == 4:
                for button in kwargs['buttons']:
                    button.config(state="disable")
        except sp.CalledProcessError as e:
            pass
 
    @staticmethod
    def status(**kwargs):
        """Comprueba el estado de un programa a traves de su demonio
        """
        try:
            sp.run(["systemctl", "is-active", kwargs['deamon']],
                        check=True, stdout=sp.DEVNULL)
            kwargs['button'].config(text="Encendido", foreground="#157936")
        except sp.CalledProcessError as e:
            if (e.returncode == 4):
                kwargs['button'].config(
                    text="No instalado", foreground="#150e0f")
            else:
                kwargs['button'].config(text="Aagado", foreground="#ff1220")

            
