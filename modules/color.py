from tkinter import ttk

class Color():

    dark = "#383c4a"
    black = "#ff2020"
    white = "#ffffff"
    blue = "#243679"
    red = "#1c6e3b"
    green = "#4c8454"
    yellow = "#b3a52d"
    red_yellow = "#d61c49"
    red_white = "#c14935"

    @staticmethod
    def set_group_color(**kwargs):
        ttk.Style().configure(kwargs["name"],relief=kwargs["relief"],foreground=kwargs['fg'], background=kwargs['bg'])
     
