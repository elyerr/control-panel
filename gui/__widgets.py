import tkinter as tk
from modules.components import Components, Color

class Server(tk.Frame):

    def __init__(self, master, tab):
        super().__init__(master)
        self.configure(background=Color.dark) 
        self.tab = tab
        
        # salida de informacion
        self.output = Components.output(self)
        
        #botones
        Components.components(master=self, name="Nginx",
                              deamon="nginx", output=self.output)
        Components.components(master=self, name="Apache",
                              deamon="httpd", output=self.output)
        Components.components(master=self, name="PHP FPM",
                              deamon="php-fpm", output=self.output)
        Components.components(master=self, name="Redis",
                              deamon="redis", output=self.output)
        Components.components(master=self, name="Libvirt KVM",
                              deamon="libvirtd", output=self.output)
        Components.components(master=self, name="Open SSH",
                              deamon="sshd", output=self.output)

        # master frame
        self.tab.add(self, text="Servidores")
 

class ServerDNS(tk.Frame):

    def __init__(self, master, tab): 
        super().__init__(master)
        self.tab = tab 
        self.configure(background=Color.dark)
         # salida de informacion
        self.output = Components.output(self)

        #####botones############
        Components.components(master=self, name="Samba Server",
                              deamon="samba", output=self.output)
        
        Components.components(master=self, name="Samba NMB",
                              deamon="nmb", output=self.output)
        
        Components.components(master=self, name="Samba SMB",
                              deamon="smb", output=self.output)

        Components.components(master=self, name="Avahi Server",
                              deamon="avahi-daemon", output=self.output) 

        Components.components(master=self, name="Bind DNS",
                              deamon="named", output=self.output)
        
        Components.components(master=self, name="DNSMASQ",
                              deamon="dnsmasq", output=self.output)

        self.tab.add(self, text="Server DNS ")


class BD(tk.Frame):

    def __init__(self, master, tab) -> None:
        super().__init__(master)
        self.tab = tab
        self.configure(background=Color.dark)
        
        # salida de informacion
        self.output = Components.output(self)

        ##### botones############
        Components.components(master=self, name="MariaDB",
                              deamon="mariadb", output=self.output)
        
        Components.components(master=self, name="Mysql",
                              deamon="mysqld", output=self.output)
        
        Components.components(master=self, name="PostgreSQL",
                              deamon="postgresql", output=self.output)
      
        tab.add(self, text="Bases de datos")


"""
"""


class Media(tk.Frame):
    def __init__(self, master, tab) -> None:
        super().__init__(master)
        self.tab = tab
        self.configure(background=Color.dark)
        # salida de informacion
        self.output = Components.output(self)

        ##### botones############
        Components.components(master=self, name="Emby Server",
                              deamon="emby-server", output=self.output)

        Components.components(master=self, name="NetworkManage",
                              deamon="NetworkManager", output=self.output)
        
        Components.components(master=self, name="IWCTL",
                              deamon="iwd", output=self.output)

        Components.components(master=self, name="DHCPCD",
                              deamon="dhcpcd", output=self.output)
        
        Components.components(master=self, name="Systemd Resolved",
                              deamon="systemd-resolved", output=self.output) 

        tab.add(self, text="Network y Media")

 

class Printers(tk.Frame):

   def __init__(self, master, tab) -> None:
       super().__init__(master)
       self.tab = tab
       self.configure(background=Color.dark)
       # salida de informacion
       self.output = Components.output(self)

       ##### botones############
       Components.components(master=self, name="UFW Firewall",
                             deamon="ufw", output=self.output)
       
       Components.components(master=self, name="Cups Printer",
                             deamon="cups", output=self.output)
       
       Components.components(master=self, name="Bluetooth",
                             deamon="bluetooth", output=self.output)
       
       tab.add(self, text="Dispositivos")
 