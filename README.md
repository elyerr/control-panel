## Control Panel
- panel de control de servicios

### Tu usuario debe pertencer a wheel
`sudo gpasswd -a your_user  wheel`

### Para instalar
`cd control_panel && ./install.py`
 
### Para remover del sistema
`cd control_panel && ./install.py -r`

### Dependencias python
- `os`
- `subprocess` 
- `psutil`
- `shutil`

### Disponibles mediante pacman
 - `tk`