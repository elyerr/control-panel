#!/usr/bin/env python3

import os
import shutil 
import sys
import subprocess

INSTALL_BIN = "/usr/local/bin"
INSTALL_SHARE = "/usr/share/control_panel"
INSTALL_RULES = "/etc/polkit-1/rules.d"
INSTALL_DESKTOP = "/usr/share/applications/cpanel.desktop"
INSTALL_ICON = "/usr/share/icons/hicolor/512x512/apps"
IGNORE_DIR = ['__pycache__','.gitignore','.git']

def ignore_files_and_dirs(_, names):
    return [name for name in names if name in IGNORE_DIR]
    
def create_desktop():
    with open(INSTALL_DESKTOP, mode="w+") as file:
        file.write("[Desktop Entry]\n")
        file.write("Name=Control Panel\n")
        file.write("Exec=cpanel\n")
        file.write("Terminal=false\n")
        file.write("Type=Application\n")
        file.write("Icon=cpanel\n")
        file.write("Comment=Administración de Servicios\n")
        file.write("GenericName= Administración de Servicios\n")
        file.write("Categories=System;Network\n")
        file.close() 
    
def remove():
    os.unlink(os.path.join(INSTALL_BIN, 'cpanel'))
    shutil.rmtree(INSTALL_SHARE)
    os.remove(INSTALL_DESKTOP)
    os.remove(os.path.join(INSTALL_RULES, '00_cpanel.rules'))
    print("Desinstalacion completa...") 

def install():       
    if os.path.exists(INSTALL_SHARE): 
        shutil.rmtree(INSTALL_SHARE)
    os.mkdir(INSTALL_SHARE)
    shutil.copy('cpanel.py', INSTALL_SHARE)
    shutil.copy('main.py', INSTALL_SHARE)
    shutil.copytree('gui', f'{INSTALL_SHARE}/gui', ignore=ignore_files_and_dirs)
    shutil.copytree('modules', f'{INSTALL_SHARE}/modules', ignore=ignore_files_and_dirs)
    shutil.copy('cpanel.png', INSTALL_ICON)
    os.chmod(os.path.join(INSTALL_SHARE, 'cpanel.py'), 0o755)

    if not os.path.exists(INSTALL_RULES):
        os.mkdir(INSTALL_RULES)
    shutil.copy('rules/00_cpanel.rules', INSTALL_RULES)
    
    if os.path.exists(os.path.join(INSTALL_BIN, 'cpanel')):
        os.unlink(os.path.join(INSTALL_BIN, 'cpanel'))
    os.symlink(os.path.join(INSTALL_SHARE, 'cpanel.py'), f'{INSTALL_BIN}/cpanel')
    
    create_desktop()
    print("Instalacion completa..!")

if __name__ == "__main__":
    if os.geteuid() != 0:
        try:
            command = f"sudo {sys.argv[0]} {sys.argv[1]}"              
        except IndexError as e:
            command = f"sudo {sys.argv[0]}"
        finally: 
            subprocess.run(command, shell=True)
            sys.exit(0)
    
    if len(sys.argv) > 1 and sys.argv[1] == '-r':
        remove()
        sys.exit()
           
    install() 
      
     