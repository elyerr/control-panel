# -*- coding: utf-8 -*-
""" 

@author: Elyerr
"""

import tkinter as tk
from tkinter import ttk     
from modules.core import System
from gui.__widgets import *
import os
import sys
        
# init funtion
def main():
    root = tk.Tk()
    root.geometry("800x640")
    root.title("Administrador de servicios") 
    root.resizable(False, False) 
    
    #event
    root.bind("<Key>", lambda event: System.close(event, root))

    ####################frame#####################################333    
    tabs = ttk.Notebook(root) 
    tabs.pack(expand=True,fill="both")
    
    #tabs
    Server(root, tabs)
    ServerDNS(root, tabs)
    BD(root, tabs)
    Media(root, tabs)
    Printers(root, tabs)
    ####################end frame#####################################333

    ###### bloquear la aplicacion ###################
    lock_file = '/tmp/cpanel.lock'

    if os.path.exists(lock_file):
        sys.exit()

    #bloquear para que no se ejecute si esta abierta
    with open(lock_file, 'w') as f:
        f.write(str(os.getpid()))
        
    root.mainloop()
